<table align="center"><tr><td align="center" width="9999"><img align="center" src="https://cdn.discordapp.com/attachments/556582916227072015/685276717295599630/unknown.png">
<h3 align="center">Projet Symfony - VueJS </h3>
</td></tr></table>



### Table of Contents 🔍

* [Les technos utilisées](#Les-technos-utilisées)
* [Les prérequis](#Les-prérequis)
* [Utilisation](#Utilisation)
* [Participation](#Participation)
* [Nous contacter](#Nous-contacter)

### Les technos utilisées 💻

* [Symfony](https://symfony.com)
* [VueJS](https://vuejs.org/)
* [Docker](https://docker.com)
* [Gitlab](https://gitlab.com)

### Les prérequis 🧩

1. PHP
2. Composer
3. npm / yarn
5. Git

### Utilisation 🐒

1. Cloner le repo
```
git clone https://gitlab.com/rverdeyme/adw
```

2. Lancer Docker
```
$ docker-compose up
```

3. Installer les composants Symfony
```
$ docker-compose exec php composer install
```

4. Installer les composants Node
```
$ docker-compose run node yarn install
```

5. Créer la base de données
```
$ docker-compose exec php bin/console d:s:u --force
```

6. Créer les data fixtures
```
$ docker-compose exec php bin/console d:f:l
```

7. Aller sur le site
```
http://localhost:8080
```

### Participation 👏
* Johnny LEITAO
* Leia JARDEL
* Léa OLIVEIRA
* Lévy ZIRE
* Rémy VERDEYME

  <br />

### Nous contacter 📧
verdeymeremy@gmail.com
