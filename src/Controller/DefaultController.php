<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/{vueRouting}", requirements={"vueRouting"="^(?!api|login_check|_(profiler|wdt)).*"}, name="index")
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('base.html.twig', []);
    }
}
