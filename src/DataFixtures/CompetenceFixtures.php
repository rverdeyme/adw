<?php


namespace App\DataFixtures;


use App\Entity\Competence;
use App\Entity\Niveau;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CompetenceFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(\Doctrine\Persistence\ObjectManager $manager)
    {

        $faker = \Faker\Factory::create();

        $niveaux = $manager->getRepository(Niveau::class)->findAll();


        $competence = (new Competence())
            ->setLibelle("Design")
            ->setNiveau($faker->randomElement($niveaux))
        ;
        $manager->persist($competence);

        $competence = (new Competence())
            ->setLibelle("Dévelopeur Web")
            ->setNiveau($faker->randomElement($niveaux))
        ;
        $manager->persist($competence);

        $competence = (new Competence())
            ->setLibelle("Digital Marketing")
            ->setNiveau($faker->randomElement($niveaux))
        ;
        $manager->persist($competence);

        $competence = (new Competence())
            ->setLibelle("Referencement SEO")
            ->setNiveau($faker->randomElement($niveaux))
        ;
        $manager->persist($competence);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            NiveauFixtures::class,
        );
    }
}