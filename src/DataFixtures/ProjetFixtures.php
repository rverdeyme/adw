<?php


namespace App\DataFixtures;


use App\Entity\Competence;
use App\Entity\Projet;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ProjetFixtures extends Fixture implements DependentFixtureInterface
{
    public function  load(\Doctrine\Persistence\ObjectManager $manager)
    {
        // TODO: Implement load() method.
        $faker = \Faker\Factory::create();
        $competences = $manager->getRepository(Competence::class)->findAll();
        $agent = $manager->getRepository(User::class)->findAll();

        for ($i = 0; $i < 10; $i++ ){
            $projet = (new Projet())
                ->setIntitule("Projet n° " . $i)
                ->setDescription($faker->sentence(20))
                ->setDateDebut($faker->dateTimeInInterval('now', '+3 months'))
                ->addStack($faker->randomElement($competences))
                ->addStack($faker->randomElement($competences))
                ->setEtat("Attente")
            ;
            $manager->persist($projet);
        }

        for ($i = 10; $i < 20; $i++ ){
            $projet = (new Projet())
                ->setIntitule("Projet n° " . $i)
                ->setDescription($faker->sentence(20))
                ->setDateDebut($faker->dateTimeInInterval('now', '+3 months'))
                ->addStack($faker->randomElement($competences))
                ->addStack($faker->randomElement($competences))
                ->addEquipe($faker->randomElement($agent))
                ->addEquipe($faker->randomElement($agent))
                ->setEtat("Finis")
            ;
            $manager->persist($projet);
        }

        for ($i = 20; $i < 30; $i++ ){
            $projet = (new Projet())
                ->setIntitule("Projet n° " . $i)
                ->setDescription($faker->sentence(20))
                ->setDateDebut($faker->dateTimeInInterval('now', '+3 months'))
                ->addStack($faker->randomElement($competences))
                ->addStack($faker->randomElement($competences))
                ->addEquipe($faker->randomElement($agent))
                ->addEquipe($faker->randomElement($agent))
                ->setEtat("En cours")
            ;
            $manager->persist($projet);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            CompetenceFixtures::class
        );
    }
}