<?php

namespace App\DataFixtures;

use App\Entity\Niveau;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class NiveauFixtures extends Fixture
{

    public function load(\Doctrine\Persistence\ObjectManager $manager)
    {
        $level = (new Niveau())->setlibelle("Junior");
        $manager->persist($level);

        $level = (new Niveau())->setlibelle("Intermediaire");
        $manager->persist($level);

        $level = (new Niveau())->setlibelle("Confirmé");
        $manager->persist($level);

        $level = (new Niveau())->setlibelle("Senior");
        $manager->persist($level);

        $manager->flush();
    }
}