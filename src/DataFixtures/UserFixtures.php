<?php


namespace App\DataFixtures;

use App\Entity\Competence;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(\Doctrine\Persistence\ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();
        $competences = $manager->getRepository(Competence::class)->findAll();

        for ($i = 0; $i < 10; $i++) {
            $user = (new User());
            $user->setNom($faker->lastName)
                ->setPrenom($faker->firstName)
                ->setEmail($faker->email)
                ->setSexe($faker->randomElement(User::SEXE))
                ->setTelephone($faker->phoneNumber)
                ->setDescription($faker->sentence(20))
                ->setRoles(["ROLE_AGENT"])
                ->setPlainPassword("string")
            ;
            $user->addCompetence($faker->randomElement($competences));


            $manager->persist($user);
        }

        for ($i = 0; $i < 10; $i++) {
            $user = (new User());
            $user->setNom($faker->lastName)
                ->setPrenom($faker->firstName)
                ->setEmail($faker->email)
                ->setSexe($faker->randomElement(User::SEXE))
                ->setTelephone($faker->phoneNumber)
                ->setDescription($faker->sentence(20))
                ->setRoles(["ROLE_CLIENT"])
                ->setPlainPassword("string")
            ;
            $user->addCompetence($faker->randomElement($competences));

            $manager->persist($user);
        }

        for ($i = 0; $i < 5; $i++) {
            $user = (new User());
            $user->setNom($faker->lastName)
                ->setPrenom($faker->firstName)
                ->setEmail($faker->email)
                ->setSexe($faker->randomElement(User::SEXE))
                ->setTelephone($faker->phoneNumber)
                ->setDescription($faker->sentence(20))
                ->setRoles(["ROLE_ADMIN"])
                ->setPlainPassword("string")
            ;
            $user->addCompetence($faker->randomElement($competences));


            $manager->persist($user);
        }


        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            CompetenceFixtures::class,
        );
    }
}