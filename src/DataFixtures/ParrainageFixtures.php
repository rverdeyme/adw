<?php


namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ParrainageFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(\Doctrine\Persistence\ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();
        $agents = $manager->getRepository(User::class)->findByRoleAgent();

        foreach ($agents as $agent){
            $agent->addParrain($faker->randomElement($agents));
            $manager->persist($agent);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }
}