<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user_account")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 * @ApiFilter(SearchFilter::class, properties={"roles": "exact" })
 */
class User implements UserInterface
{

    const SEXE = [
        'Homme' => 'H',
        'Femme' => 'F',
        'Autres' => 'A',
    ];


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read","write"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"read","write"})
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Groups({"read","write"})
     * @ApiSubresource()
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @Groups({"write"})
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string plainPassword
     * @Groups({"write"})
     */
    private $plainPassword;


    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read","write"})
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read","write"})
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read","write"})
     */
    private $sexe;


    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read","write"})
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read","write"})
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isVerified;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Projet", mappedBy="equipe")
     * @Groups({"read","write"})
     */
    private $projets;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read","write"})
     */
    private $dateNaissance;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="filleuls", cascade={"persist"})
     * @Groups({"read","write"})
     */
    private $parrains;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="parrains", cascade={"persist"})
     * @Groups({"read","write"})
     */
    private $filleuls;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read","write"})
     */
    private $cv;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Competence", inversedBy="agents")
     */
    private $competences;

    public function __construct()
    {
        $this->isVerified = false;
        $this->projets = new ArrayCollection();
        $this->parrains = new ArrayCollection();
        $this->filleuls = new ArrayCollection();
        $this->competences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
//         If you store any temporary, sensitive data on the user, clear it here
         $this->plainPassword = "";
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * @param mixed $dateNaissance
     */
    public function setDateNaissance($dateNaissance): void
    {
        $this->dateNaissance = $dateNaissance;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @return Collection|Projet[]
     */
    public function getProjets(): Collection
    {
        return $this->projets;
    }

    public function addProjet(Projet $projet): self
    {
        if (!$this->projets->contains($projet)) {
            $this->projets[] = $projet;
            $projet->addEquipe($this);
        }

        return $this;
    }

    public function removeProjet(Projet $projet): self
    {
        if ($this->projets->contains($projet)) {
            $this->projets->removeElement($projet);
            $projet->removeEquipe($this);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword(): string
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     */
    public function setPlainPassword(string $plainPassword): void
    {
        if ($plainPassword != null){
            $this->plainPassword = $plainPassword;
        }
    }

    /**
     * @return Collection|self[]
     */
    public function getParrains(): Collection
    {
        return $this->parrains;
    }

    public function addParrain(self $parrain): self
    {
        if (!$this->parrains->contains($parrain)) {
            $this->parrains[] = $parrain;
        }

        return $this;
    }

    public function removeParrain(self $parrain): self
    {
        if ($this->parrains->contains($parrain)) {
            $this->parrains->removeElement($parrain);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getFilleuls(): Collection
    {
        return $this->filleuls;
    }

    public function addFilleul(self $filleul): self
    {
        if (!$this->filleuls->contains($filleul)) {
            $this->filleuls[] = $filleul;
            $filleul->addParrain($this);
        }

        return $this;
    }

    public function removeFilleul(self $filleul): self
    {
        if ($this->filleuls->contains($filleul)) {
            $this->filleuls->removeElement($filleul);
            $filleul->removeParrain($this);
        }

        return $this;
    }

    public function getCv(): ?string
    {
        return $this->cv;
    }

    public function setCv(?string $cv): self
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * @return Collection|Competence[]
     */
    public function getCompetences(): Collection
    {
        return $this->competences;
    }

    public function addCompetence(Competence $competence): self
    {
        if (!$this->competences->contains($competence)) {
            $this->competences[] = $competence;
        }

        return $this;
    }

    public function removeCompetence(Competence $competence): self
    {
        if ($this->competences->contains($competence)) {
            $this->competences->removeElement($competence);
        }

        return $this;
    }

}
