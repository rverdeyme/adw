<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *     subresourceOperations={
 *          "api_projets_equipe_get_subresource"={
 *          "method"="GET",
 *          "normalization_context"={"groups"={"read"}}
 *      }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ProjetRepository")
 * @ApiFilter(SearchFilter::class, properties={"etat": "exact", "equipe.id": "exact" })
 */
class Projet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $intitule; 

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etat;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_debut;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_fin;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Competence", inversedBy="projets")
     */
    private $stack;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="projets")
     * @ApiSubresource()
     */
    private $equipe;

    public function __construct()
    {
        $this->stack = new ArrayCollection();
        $this->equipe = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->date_debut;
    }

    public function setDateDebut(?\DateTimeInterface $date_debut): self
    {
        $this->date_debut = $date_debut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->date_fin;
    }

    public function setDateFin(\DateTimeInterface $date_fin): self
    {
        $this->date_fin = $date_fin;

        return $this;
    }

    /**
     * @return Collection|Competence[]
     */
    public function getStack(): Collection
    {
        return $this->stack;
    }

    public function addStack(Competence $stack): self
    {
        if (!$this->stack->contains($stack)) {
            $this->stack[] = $stack;
        }

        return $this;
    }

    public function removeStack(Competence $stack): self
    {
        if ($this->stack->contains($stack)) {
            $this->stack->removeElement($stack);
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getEquipe(): Collection
    {
        return $this->equipe;
    }

    public function addEquipe(User $equipe): self
    {
        if (!$this->equipe->contains($equipe)) {
            $this->equipe[] = $equipe;
        }

        return $this;
    }

    public function removeEquipe(User $equipe): self
    {
        if ($this->equipe->contains($equipe)) {
            $this->equipe->removeElement($equipe);
        }

        return $this;
    }
}
