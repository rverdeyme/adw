<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class JWTCreatedListener{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * JWTCreatedListener constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        /** @var \App\Entity\User $currentUser */
        $currentUser = $this->tokenStorage->getToken()->getUser();

        $payload       = $event->getData();
        $payload['id'] = $currentUser->getId();

        $event->setData($payload);
    }
}