import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home";
import PublicPage from "../views/PublicPage";
import Signin from "../views/Signin";
import Projects from "../views/Projects";
import Profile from "../views/Profile";
import Ranking from "../views/Ranking"
import FinishProjects from "../views/FinishProjects";
import ProgressProjects from "../views/ProgressProjects";
import Parrainage from "../views/Parrainage";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  routes: [
    { path: "/home", component: Home },
    { path: "/", component: PublicPage },
    { path: "/signin", component: Signin },
    { path: "/projects", component: Projects},
    { path: "/finished", component: FinishProjects},
    { path: "/inprogress", component: ProgressProjects},
    { path: "/profile", component: Profile},
    { path: "/ranking", component: Ranking},
    { path: "/sponsorship", component: Parrainage},
    { path: "*", redirect: "/home" }
  ]
});