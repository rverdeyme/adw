import Vue from "vue";
import App from "./App";
import router from "./router";
import VueScrollmagic from 'vue-scrollmagic'
import gsap from 'gsap'
import vueMoment from 'vue-moment'

Vue.use(vueMoment);
Vue.use(gsap);
Vue.use(VueScrollmagic);

new Vue({
  components: { App },
  template: "<App/>",
  router
}).$mount("#app");

